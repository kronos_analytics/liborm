<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__) . '/exception.class.php';


interface ICustomProperty
{
	public function setName($sName);
	public function setValue($mixedValue);
}


/**
 * This class represent a custom property.
 * A custom property have a name and a value
 *
 */
class ORM_CustomProperty implements ICustomProperty
{
	private $_name = null;
	
	private $_value = null;

	
	/**
	 * Constructor
	 *
	 * @param string	$sName		Name of the property
	 * @param mixed		$mixedValue	Value of the property
	 * 
	 * @throws ORM_IllegalArgumentException if $sName is not valid.
	 */
	public function __construct($sName, $mixedValue = null)
	{
		$this->setName($sName);
		$this->setValue($mixedValue);
	}
	
	
	
	/**
	 * Get the custom property name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->_name;
	}
	
	
	/**
	 * Set the custom property name
	 *
	 * @param string $sName Name of the custom property
	 * 
 	 * @throws ORM_IllegalArgumentException if $sName is not valid.
	 */
	public function setName($sName)
	{
		if (!is_string($sName)) {
			$sError = 'The property name must be a string';
			throw new ORM_IllegalArgumentException($sError);
		}
		
		if (0 == strlen(trim($sName))) {
			$sError = 'The property name must not be empty';
			throw new ORM_IllegalArgumentException($sError);
		}
		
		$this->_name = $sName;
	}
	
	
	/**
	 * Get the value of the custom property
	 *
	 * @return mixed
	 */
	public function getValue()
	{
		return $this->_value;
	}
	
	
	/**
	 * Set the value of the custom property
	 *
	 * @param mixed $mixedValue Value of the custom property
	 */
	public function setValue($mixedValue)
	{
		$this->_value = $mixedValue;
	}
}



interface ICustomProperties
{
	public function set($sName, $mixedValue);
	public function get($sName);
}


/**
 * This class is a collection of custom property
 *
 */
class ORM_CustomProperties implements ICustomProperties
{
    /**
     * @var array
     */
	private $_customProperty = array();
	
	/**
	 * @param	array	$properties		list of allowed properties in the collection
	 * 									can contain string or ORM_CustomProperty objects
	 */
	public function __construct($properties = null)
	{
		if (null !== $properties) {
			foreach ($properties as $mixedValue) {
				$this->addProperty($mixedValue);
			}
		}
	}

	
	/**
	 * Add property to the collection.
	 * @param string | ORM_CustomProperty $mixedValue
	 * @return ORM_CustomProperties
	 */
	public function addProperty($mixedValue)
	{
		if ($mixedValue instanceof ORM_CustomProperty) {
			$this->_customProperty[$mixedValue->getName()] = $mixedValue;
		} elseif (strlen(trim($mixedValue)) > 0) {
			$this->_customProperty[$mixedValue] = new ORM_CustomProperty($mixedValue);
		}
		
		return $this;
	}
	
	

	/**
	 * Adds properties to the collection.
	 * Takes a variable number of ORM_CustomProperty or string parameters.
	 * If the parameter is a string so a new ORM_CustomProperty named with 
	 * the value of the string parameter will be added
	 *
	 * @param ORM_CustomProperty|string		$oCustomProperty1, $sPropertyName2 ...	Unlimited number of parameters
	 * 
	 * @throws ORM_IllegalArgumentException if the property name is not valid.
	 */
	protected function create()
	{
		$aArgList = func_get_args();
		$iNumArgs = func_num_args();
		if (0 < $iNumArgs) {
			foreach ($aArgList as $mixedValue) {
				$this->addProperty($mixedValue);
			}
		}
	}
	
	
	/**
	 * Retun a value that indicate if a property exists
	 *
	 * @param string $sName	Name of the property
	 * @return bool			True if the property exists, false otherwise
	 */
	public function propertExits($sName)
	{
		return array_key_exists($sName, $this->_customProperty);
	}
	
	
	//ICustomProperties implementation
	
	
	/**
	 * Get the value of a property
	 *
	 * @param string $sName	Name of the property
	 * 
	 * @throws ORM_OutOfBoundException if the property does not exists.
	 * 
	 * @return mixed
	 */
	public function get($sName)
	{
		if (!$this->propertExits($sName)) {
			$sError = 'The property ' . $sName  . ' does not exists';
			throw new ORM_OutOfBoundException($sError);
		}
		
		return $this->_customProperty[$sName]->getValue();
	}
	
	
	/**
	 * Set the value of a property
	 *
	 * @param string	$sName		Name of the property
	 * @param mixed 	$mixedValue	Value of the property
	 * 
	 * @throws ORM_OutOfBoundException if the property does not exists.
	 */
	public function set($sName, $mixedValue)
	{
		if (!$this->propertExits($sName)) {
			$sError = 'The property ' . $sName  . ' does not exists';
			throw new ORM_OutOfBoundException($sError);
		}
		
		$this->_customProperty[$sName]->setValue($mixedValue);
	}
}
