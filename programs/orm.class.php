<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */



class Func_LibOrm extends bab_Functionality
{

	/**
	 * Returns the functionality description.
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return bab_translate('Object-relational mapping');
	}

	/**
	 *
	 */
	public function init()
	{
	    require_once dirname(__FILE__) . '/backend.class.php';
	    require_once dirname(__FILE__) . '/criteria.class.php';
	    require_once dirname(__FILE__) . '/customProperty.class.php';
	    require_once dirname(__FILE__) . '/exception.class.php';
	    require_once dirname(__FILE__) . '/field.class.php';
	    require_once dirname(__FILE__) . '/iterator.class.php';
	    require_once dirname(__FILE__) . '/set.class.php';
	}

	/**
	 *
	 */
	public function initMysql()
	{
	    $this->init();
		require_once dirname(__FILE__) . '/MySql/backend.class.php';
		require_once dirname(__FILE__) . '/MySql/iterator.class.php';
		require_once dirname(__FILE__) . '/MySql/relation.class.php';
		require_once dirname(__FILE__) . '/MySql/interface.class.php';
		require_once dirname(__FILE__) . '/MySql/set.class.php';
	}
}
