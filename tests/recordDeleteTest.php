<?php

require_once dirname(__FILE__) . '/mock/core.php';


class ORM_RecordDeleteTest extends PHPUnit_Framework_TestCase
{

    protected function getRecordSet()
    {
        $set = new ORM_TestRecordSet();

        return $set;
    }


   public function setUp()
   {
       global $babDB;
       $babDB->db_query('TRUNCATE orm_testrecord');
   }

   public function tearDown()
   {
   }


   public function testSave()
   {
       $set = $this->getRecordSet();
       $record = $set->newRecord();

       $record->name = 'test';
       $this->assertTrue($record->save());

       $this->assertEquals('1', $record->id); // AUTO INCREMENT should return 1 with new table ?
   }


   public function testDeleteRecord()
   {
       $this->testSave();

       $set = $this->getRecordSet();
       $record = $set->get(1);

       $this->assertInstanceOf('ORM_TestRecord', $record);
       $this->assertTrue($record->delete());

       $this->assertEquals(null, $set->get(1));
   }
}