<?php


require_once dirname(__FILE__) . '/mock/core.php';


class ORM_fieldKeyTest extends PHPUnit_Framework_TestCase
{
    public function testKeysCreation()
    {
        $set = new ORM_MockFieldKeySet();
        $keys = $set->getFieldKeys();

        $this->assertCount(2, $keys);
    }


    public function testMysqlBackend()
    {
        $set = new ORM_MockFieldKeySet();
        $backend = $set->getBackend();

        $sql = $backend->setToSql($set);

        $this->assertRegExp('/UNIQUE KEY/', $sql);
    }

    public function testFieldIndexMethodLinkedToSet()
    {
        $set = new ORM_MockSet;
        $field = $set->getField('int')->index();
        $this->assertInstanceOf('ORM_IntField', $field);

        $keys = $set->getFieldKeys();

        $this->assertCount(1, $keys);

        $this->assertFalse($keys[0]->unique);
    }


    public function testFieldIndexMethodOrphan()
    {
        $set = new ORM_MockSet;
        $field = ORM_IntField('dummy')->index();
        $this->assertInstanceOf('ORM_IntField', $field);

        $keys = $set->getFieldKeys();

        $this->assertCount(0, $keys);
        $set->addFields($field);

        $keys = $set->getFieldKeys();
        $this->assertCount(1, $keys);

        $this->assertFalse($keys[0]->unique);
    }


    public function testFieldUniqueMethodLinkedToSet()
    {
        $set = new ORM_MockSet;
        $field = $set->getField('int')->unique();
        $this->assertInstanceOf('ORM_IntField', $field);

        $keys = $set->getFieldKeys();

        $this->assertCount(1, $keys);

        $this->assertTrue($keys[0]->unique);
    }


    public function testFieldUniqueMethodOrphan()
    {
        $set = new ORM_MockSet;
        $field = ORM_IntField('dummy')->unique();
        $this->assertInstanceOf('ORM_IntField', $field);

        $keys = $set->getFieldKeys();

        $this->assertCount(0, $keys);
        $set->addFields($field);

        $keys = $set->getFieldKeys();
        $this->assertCount(1, $keys);


        $this->assertTrue($keys[0]->unique);
    }
}
