<?php



class ORM_TestRecordSet extends ORM_RecordSet
{
    public function __construct()
    {
        parent::__construct();
        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name')
        );
    }
}

class ORM_TestRecord extends ORM_Record
{
}
