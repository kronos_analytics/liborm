<?php



/**
 *
 * @property ORM_PkField 		$id
 * @property ORM_StringField 	$string
 * @property ORM_TextField 		$text
 * @property ORM_IntField 		$int
 * @property ORM_DecimalField 	$decimal
 * @property ORM_CurrencyField 	$currency
 * @property ORM_EmailField 	$email
 * @property ORM_DateField 		$date
 * @property ORM_DatetimeField 	$bool
 * @property ORM_EnumField 		$enum
 * @property ORM_HtmlField 		$html
 * @property ORM_TimeField 		$time
 * @property ORM_UrlField 		$url
 * @property ORM_UserField 		$user
 */
class ORM_MockSet extends ORM_RecordSet
{
    /**
     * Mock set
     */
	public function __construct()
	{
		parent::__construct();

		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('string'),
			ORM_TextField('text'),
			ORM_IntField('int'),
			ORM_DecimalField('decimal', 3),
			ORM_CurrencyField('currency'),
			ORM_EmailField('email'),
			ORM_DateField('date')->setNullAllowed(true),
		    ORM_DatetimeField('datetime')->setNullAllowed(true),
			ORM_BoolField('bool'),
			ORM_EnumField('enum', array(1 => 'v1', 2 => 'v2')),
			ORM_HtmlField('html'),
		    ORM_TimeField('time')->setNullAllowed(true),
			ORM_UrlField('url'),
			ORM_UserField('user')
		);

		$this->hasOne('parent', 'ORM_MockSet');

	}
}

class ORM_Mock extends ORM_Record
{
}
