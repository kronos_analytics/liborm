<?php

require_once dirname(__FILE__) . '/stringFieldTest.php';

class ORM_phoneFieldTest extends ORM_stringFieldTest
{
    protected $fieldClass = 'ORM_PhoneField';


    public function testOutputWidgetIsDisplayable()
    {
        $field = $this->construct('display');

        $item = $field->outputWidget(null);
        $this->assertInstanceOf('Widget_Displayable_Interface', $item);

        $item = $field->outputWidget('0123456789');
        $this->assertInstanceOf('Widget_Displayable_Interface', $item);
    }
}
