<?php

require_once dirname(__FILE__) . '/fieldTest.php';

class ORM_TimeFieldTest extends ORM_fieldTest
{
    protected $fieldClass = 'ORM_TimeField';


    public function testIsValueSetWithNullAllowed()
    {
        $field = $this->construct('time');
        $field->setNullAllowed(true);

        $this->assertFalse($field->isValueSet(null));
        $this->assertTrue($field->isValueSet(''));
        $this->assertTrue($field->isValueSet(false));
        $this->assertTrue($field->isValueSet(0));
        $this->assertTrue($field->isValueSet(ORM_TimeField::EMPTY_TIME));
    }

    public function testIsValueSetWithNullNotAllowed()
    {
        $field = $this->construct('time');
        $field->setNullAllowed(false);

        $this->assertFalse($field->isValueSet(null), 'Null value is not set');
        $this->assertFalse($field->isValueSet(''), 'an empty string is not a set datetime if null value not allowed');
        $this->assertFalse($field->isValueSet(false));
        $this->assertFalse($field->isValueSet(ORM_TimeField::EMPTY_TIME));
        $this->assertTrue($field->isValueSet('00:00:01'));
    }
}
